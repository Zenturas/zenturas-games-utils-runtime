/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.statics.ZConfiguration;
import de.zenturas.games.utils.runtime.statics.ZUtils;

/**
 * @version 0.0.0.0
 */
class InternalStage extends Stage {
    private static final float BORDER_OFFSET = 0.5f;
    private boolean isDebugActive;
    private float parentAlpha;
    private InternalViewport2D viewport;

    public InternalStage(@NotNull InternalViewport2D viewport) {
        super(viewport.viewport, ZUtils.GetSpriteBatch());
        this.viewport = viewport;
        isDebugActive = false;
        parentAlpha = 1.0f;
    }

    @Override
    public void draw() {
        Group group = getRoot();
        if (group.isVisible() && getActors().size > 0) {
            Batch batch = getBatch();
            batch.setProjectionMatrix(getCamera().combined);
            batch.begin();
            group.draw(batch, parentAlpha);
            batch.end();
        }
    }

    public void drawDebug() {
        boolean useDebug2D = ZConfiguration.UseDebug2D;
        if (useDebug2D || isDebugActive) {
            ShapeRenderer renderer = ZUtils.GetShapeRenderer();
            renderer.setProjectionMatrix(getCamera().combined);
            renderer.begin(ShapeType.Line);
            if (useDebug2D) {
                Group group = getRoot();
                group.setDebug(true, true);
                group.drawDebug(renderer);
            }
            if (isDebugActive) {
                renderer.setColor(Color.NAVY);
                renderer.rect(BORDER_OFFSET, BORDER_OFFSET, viewport.getVirtualWidth() - BORDER_OFFSET, viewport.getVirtualHeight() - BORDER_OFFSET);
            }
            renderer.end();
        }
    }

    public float getParentAlpha() {
        return parentAlpha;
    }

    public boolean isDebugActive() {
        return isDebugActive;
    }

    public void setDebugActive(boolean isDebugActive) {
        this.isDebugActive = isDebugActive;
    }

    /**
     * @param parentAlpha [0.0, 1.0]
     */
    public void setParentAlpha(float parentAlpha) {
        this.parentAlpha = ZUtils.Clamp(parentAlpha);
    }
}

