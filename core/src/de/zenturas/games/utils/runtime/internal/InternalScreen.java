/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.InputProcessor;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.core.InternalCoreToken;
import de.zenturas.games.utils.runtime.statics.ZUtils;

/**
 * @version 0.0.5.4
 */
public abstract class InternalScreen implements InputProcessor {
    public InternalScreen(@NotNull InternalCoreToken token) {
        token.isNull();
    }

    void renderScreen() {
        ZUtils.ClearScreen();
        render();
    }

    protected abstract void create();
    protected abstract void dispose();
    protected abstract void pause();
    protected abstract void render();
    protected abstract void resize(int width, int height);
    protected abstract void resume();
    protected abstract void update(float delta);
}
