/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

import de.zenturas.games.utils.runtime.anotations.NotNull;

/**
 * @version 0.0.1.7
 */
abstract class InternalCamera2D {
    OrthographicCamera camera;
    private boolean isDirty;

    public InternalCamera2D() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false);
        isDirty = true;
    }

    public void addViewportPosition(float x, float y) {
        camera.position.add(x, y, 0.0f);
        isDirty = true;
    }

    public void addViewportX(float x) {
        camera.position.add(x, 0.0f, 0.0f);
        isDirty = true;
    }

    public void addViewportY(float y) {
        camera.position.add(0.0f, y, 0.0f);
        isDirty = true;
    }

    public void center() {
        camera.position.set(camera.viewportWidth * 0.5f, camera.viewportHeight * 0.5f, 0.0f);
        isDirty = true;
    }

    public float getViewportHeight() {
        return camera.viewportHeight;
    }

    public float getViewportWidth() {
        return camera.viewportWidth;
    }

    public float getViewportX() {
        return camera.position.x - camera.viewportWidth * 0.5f;
    }

    public float getViewportY() {
        return camera.position.y - camera.viewportHeight * 0.5f;
    }

    public void setViewportPosition(float x, float y) {
        camera.position.set(x + camera.viewportWidth * 0.5f, y + camera.viewportHeight * 0.5f, 0.0f);
        isDirty = true;
    }

    public void setViewportX(float x) {
        camera.position.x = x + camera.viewportWidth * 0.5f;
        isDirty = true;
    }

    public void setViewportY(float y) {
        camera.position.y = y + camera.viewportHeight * 0.5f;
        isDirty = true;
    }

    public void unproject(@NotNull Vector3 out) {
        camera.unproject(out);
    }

    void updateCamera(float delta) {
        if (isDirty) {
            camera.update();
            isDirty = false;
        }
    }
}

