/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.core.ZGame;
import de.zenturas.games.utils.runtime.statics.ZUtils;

/**
 * @version 0.0.5.7
 */
public final class InternalApplication implements ApplicationListener {
    private InternalGame game;
    private boolean isDisposing;

    private InternalApplication(@NotNull ZGame game) {
        this.game = game;
    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_NONE);
        isDisposing = false;
        game.create();
    }

    @Override
    public void dispose() {
        isDisposing = true;
        game.disposeGame();
    }

    @Override
    public void pause() {
        if (!isDisposing)
            game.pauseGame();
    }

    @Override
    public void render() {
        if (!isDisposing) {
            game.updateGame(Math.min(ZUtils.GetDeltaTime(), 0.03333f));
            game.renderGame();
        }
    }

    @Override
    public void resize(int width, int height) {
        if (!isDisposing && Math.max(0, width) != 0 && Math.max(0, height) != 0)
            game.resizeGame(width, height);
    }

    @Override
    public void resume() {
        if (!isDisposing)
            game.resumeGame();
    }
}
