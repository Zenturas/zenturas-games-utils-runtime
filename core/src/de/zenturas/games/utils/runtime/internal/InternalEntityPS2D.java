/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.entities2D.InternalEntities2DToken;

/**
 * @version 0.0.0.0
 */
public abstract class InternalEntityPS2D extends InternalEntityP2D {
    public InternalEntityPS2D(@NotNull InternalEntities2DToken token) {
        super(token);
    }

    public float getHeight() {
        return actor.getHeight();
    }

    public float getWidth() {
        return actor.getWidth();
    }

    /**
     * @param height [0.0, {@link Float#MAX_VALUE}]
     */
    public void setHeight(float height) {
        actor.setHeight(height < 0.0f ? 0.0f : height);
    }

    /**
     * @param width [0.0, {@link Float#MAX_VALUE}]
     * @param height [0.0, {@link Float#MAX_VALUE}]
     */
    public void setSize(float width, float height) {
        actor.setSize(width < 0.0f ? 0.0f : width, height < 0.0f ? 0.0f : height);
    }

    /**
     * @param width [0.0, {@link Float#MAX_VALUE}]
     */
    public void setWidth(float width) {
        actor.setWidth(width < 0.0f ? 0.0f : width);
    }
}
