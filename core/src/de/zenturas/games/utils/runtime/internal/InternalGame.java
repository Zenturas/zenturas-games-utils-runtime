/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.core.InternalCoreToken;
import de.zenturas.games.utils.runtime.core.ZScreen;
import de.zenturas.games.utils.runtime.statics.ZLifecycle;
import de.zenturas.games.utils.runtime.statics.ZUtils;

/**
 * @version 0.0.3.6
 */
public abstract class InternalGame {
    private static InternalScreen CurrentScreen = null;

    public InternalGame(@NotNull InternalCoreToken token) {
        token.isNull();
    }

    public static void SetCurrentScreen(@NotNull ZScreen currentScreen) {
        if (CurrentScreen != null) {
            CurrentScreen.pause();
            CurrentScreen.dispose();
        }
        CurrentScreen = currentScreen;
        CurrentScreen.create();
        CurrentScreen.resize(ZUtils.GetScreenWidth(), ZUtils.GetScreenHeight());
    }

    void disposeGame() {
        if (CurrentScreen != null)
            CurrentScreen.dispose();
        CurrentScreen = null;
        dispose();
        ZLifecycle.InternalDispose(InternalToken.TOKEN);
    }

    void pauseGame() {
        pause();
        if (CurrentScreen != null)
            CurrentScreen.pause();
        ZLifecycle.InternalPause(InternalToken.TOKEN);
    }

    void renderGame() {
        render();
        if (CurrentScreen != null)
            CurrentScreen.renderScreen();
        ZLifecycle.InternalRender(InternalToken.TOKEN);
    }

    void resizeGame(int width, int height) {
        resize(width, height);
        if (CurrentScreen != null)
            CurrentScreen.resize(width, height);
        ZLifecycle.InternalResize(InternalToken.TOKEN, width, height);
    }

    void resumeGame() {
        resume();
        if (CurrentScreen != null)
            CurrentScreen.resume();
        ZLifecycle.InternalResume(InternalToken.TOKEN);
    }

    void updateGame(float delta) {
        update(delta);
        if (CurrentScreen != null)
            CurrentScreen.update(delta);
        ZLifecycle.InternalUpdate(InternalToken.TOKEN, delta);
    }

    protected abstract void create();
    protected abstract void dispose();
    protected abstract void pause();
    protected abstract void render();
    protected abstract void resize(int width, int height);
    protected abstract void resume();
    protected abstract void update(float delta);
}
