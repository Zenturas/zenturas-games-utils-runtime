/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.scene2D.InternalScene2DToken;

/**
 * @version 0.0.5.8
 */
public class InternalStage2D extends InternalViewport2D {
    private InternalStage stage;

    /**
     * @param token *
     * @param virtualWidth [{@link #MIN_WIDTH}, {@link Float#MAX_VALUE}]
     * @param virtualHeight [{@link #MIN_HEIGHT}, {@link Float#MAX_VALUE}]
     */
    public InternalStage2D(@NotNull InternalScene2DToken token, float virtualWidth, float virtualHeight) {
        super(virtualWidth, virtualHeight);
        token.isAllowedToUse();
        stage = new InternalStage(this);
        center();
    }

    public void addActor(@NotNull Actor actor) {
        stage.addActor(actor);
    }

    public void addEntity(@NotNull InternalEntity2D entity) {
        entity.addStage(stage);
    }

    public void clear() {
        stage.clear();
    }

    public void dispose() {
        stage.dispose();
    }

    public int getActorCount() {
        return stage.getActors().size;
    }

    public float getParentAlpha() {
        return stage.getParentAlpha();
    }

    public boolean isDebugActive() {
        return stage.isDebugActive();
    }

    public void render() {
        renderViewport();
        stage.draw();
    }

    public void renderDebug() {
        stage.drawDebug();
    }

    public void resize(int width, int height) {
        resizeViewport(width, height);
    }

    public void setDebugActive(boolean isDebugActive) {
        stage.setDebugActive(isDebugActive);
    }

    public void setInputListener(@NotNull InputMultiplexer multiplexer) {
        multiplexer.addProcessor(stage);
    }

    /**
     * @param parentAlpha [0.0, 1.0]
     */
    public void setParentAlpha(float parentAlpha) {
        stage.setParentAlpha(parentAlpha);
    }

    public void update(float delta) {
        updateViewport(delta);
        stage.act(delta);
    }

    @Override
    void setViewport() {
        if (stage != null)
            stage.setViewport(viewport);
    }
}
