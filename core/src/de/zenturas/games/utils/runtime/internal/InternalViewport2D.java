/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.zenturas.games.utils.runtime.statics.ZUtils;

/**
 * @version 0.0.3.6
 */
abstract class InternalViewport2D extends InternalCamera2D {
    private static final float MIN_HEIGHT = 1.0f;
    private static final float MIN_WIDTH = 1.0f;
    Viewport viewport;
    private boolean isCameraCentered;
    private boolean isDirty;
    private float virtualHeight;
    private float virtualWidth;

    /**
     * @param virtualWidth [{@link #MIN_WIDTH}, {@link Float#MAX_VALUE}]
     * @param virtualHeight [{@link #MIN_HEIGHT}, {@link Float#MAX_VALUE}]
     */
    public InternalViewport2D(float virtualWidth, float virtualHeight) {
        super();
        this.virtualHeight = Math.max(MIN_HEIGHT, virtualHeight);
        this.virtualWidth = Math.max(MIN_WIDTH, virtualWidth);
        isCameraCentered = true;
        toExtendViewport();
    }

    @Override
    public void addViewportPosition(float x, float y) {
        if (!isCameraCentered)
            super.addViewportPosition(x, y);
    }

    @Override
    public void addViewportX(float x) {
        if (!isCameraCentered)
            super.addViewportX(x);
    }

    @Override
    public void addViewportY(float y) {
        if (!isCameraCentered)
            super.addViewportY(y);
    }

    @Override
    public void center() {
        super.center();
        if (viewport instanceof ExtendViewport)
            super.addViewportPosition((getViewportWidth() - virtualWidth) * -0.5f, (getViewportHeight() - virtualHeight) * -0.5f);
    }

    public float getVirtualHeight() {
        return viewport instanceof ExtendViewport ? virtualHeight : viewport.getWorldHeight();
    }

    public float getVirtualWidth() {
        return viewport instanceof ExtendViewport ? virtualWidth : viewport.getWorldWidth();
    }

    public boolean isCameraCentered() {
        return isCameraCentered;
    }

    public void setCameraCentered(boolean isCameraCentered) {
        this.isCameraCentered = isCameraCentered;
        isDirty = true;
    }

    @Override
    public void setViewportPosition(float x, float y) {
        if (!isCameraCentered)
            super.setViewportPosition(x, y);
    }

    @Override
    public void setViewportX(float x) {
        if (!isCameraCentered)
            super.setViewportX(x);
    }

    @Override
    public void setViewportY(float y) {
        if (!isCameraCentered)
            super.setViewportY(y);
    }

    public void toExtendViewport() {
        if (viewport == null || viewport instanceof ScreenViewport) {
            viewport = new ExtendViewport(virtualWidth, virtualHeight, camera);
            isDirty = true;
            updateViewport(0.0f);
            setViewport();
        }
    }

    public void toScreenViewport() {
        if (viewport == null || viewport instanceof ExtendViewport) {
            viewport = new ScreenViewport(camera);
            isDirty = true;
            updateViewport(0.0f);
            setViewport();
        }
    }

    void renderViewport() {
        viewport.apply();
    }

    void resizeViewport(int width, int height) {
        viewport.update(width, height, false);
        if (isCameraCentered)
            center();
    }

    void updateViewport(float delta) {
        updateCamera(delta);
        if (isDirty) {
            viewport.update(ZUtils.GetScreenWidth(), ZUtils.GetScreenHeight(), false);
            isDirty = false;
            if (isCameraCentered)
                center();
        }
    }

    abstract void setViewport();
}
