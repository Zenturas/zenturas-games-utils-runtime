/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.zenturas.games.utils.runtime.anotations.NotNull;

/**
 * @version 0.0.0.0
 */
class InternalActor extends Actor {
    private InternalEntity2D entity;

    public InternalActor(@NotNull InternalEntity2D entity) {
        this.entity = entity;
    }

    @Override
    public void act(float delta) {
        if (isVisible())
            entity.update(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (isVisible())
            entity.render(batch, parentAlpha);
    }
}
