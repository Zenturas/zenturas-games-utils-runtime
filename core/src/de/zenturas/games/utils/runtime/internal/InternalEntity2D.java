/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.internal;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.entities2D.InternalEntities2DToken;

/**
 * @version 0.0.7.0
 */
public abstract class InternalEntity2D {
    InternalActor actor;

    public InternalEntity2D(@NotNull InternalEntities2DToken token) {
        token.isAllowedToUse();
        actor = new InternalActor(this);
    }

    public void addGroup(@NotNull Group group) {
        group.addActor(actor);
    }

    public void addListener(@NotNull EventListener listener) {
        actor.addListener(listener);
    }

    public void addStage(@NotNull Stage stage) {
        stage.addActor(actor);
    }

    public void clearListeners() {
        actor.clearListeners();
    }

    public Color getColor() {
        return actor.getColor();
    }

    public Touchable getTouchable() {
        return actor.getTouchable();
    }

    public boolean isVisible() {
        return actor.isVisible();
    }

    public void remove() {
        actor.remove();
    }

    public boolean removeListener(@NotNull EventListener listener) {
        return actor.removeListener(listener);
    }

    public void setColor(@NotNull Color color) {
        actor.setColor(color);
    }

    public void setTouchable(@NotNull Touchable touchable) {
        actor.setTouchable(touchable);
    }

    public void setVisible(boolean isVisible) {
        actor.setVisible(isVisible);
    }

    /**
     * @param batch *
     * @param parentAlpha [0.0, 1.0]
     */
    protected abstract void render(@NotNull Batch batch, float parentAlpha);
    protected abstract void update(float delta);
}
