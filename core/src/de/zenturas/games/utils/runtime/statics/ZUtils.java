/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.GLVersion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.RandomXS128;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.interfaces.IDispose;
import de.zenturas.games.utils.runtime.manager.ZAssetManager;

/**
 * @version 0.0.9.3
 */
public final class ZUtils {
    private static final Color BACKGROUND_COLOR = new Color(Color.WHITE);
    private static final String PROPERTY_LINE_SEPARATOR = "line.separator";
    private static final String PROPERTY_TAB = "\t";
    private static String FileSeparator = null;
    private static long ID = 0;
    private static String LineSeparator = null;
    private static ShapeRenderer ShapeRenderer = null;
    private static SpriteBatch SpriteBatch = null;
    private static String Tab = null;

    private ZUtils() {
    }

    public static float Clamp(float value) {
        return MathUtils.clamp(value, 0.0f, 1.0f);
    }

    public static void ClearScreen() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(BACKGROUND_COLOR.r, BACKGROUND_COLOR.g, BACKGROUND_COLOR.b, BACKGROUND_COLOR.a);
    }

    public static void Exit() {
        Gdx.app.exit();
    }

    public static ApplicationType GetApplicationType() {
        return Gdx.app.getType();
    }

    public static ZAssetManager GetAssetManager() {
        return ZAssetManager.GetAssetManager();
    }

    public static String GetCurrentDate() {
        return SimpleDateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
    }

    public static float GetDeltaTime() {
        return Gdx.graphics.getDeltaTime();
    }

    public static String GetFileSeparator() {
        if (FileSeparator == null)
            FileSeparator = File.separator;
        return FileSeparator;
    }

    public static GLVersion GetGLVersion() {
        return Gdx.graphics.getGLVersion();
    }

    public static long GetJavaHeap() {
        return Gdx.app.getJavaHeap();
    }

    public static String GetLineSeparator() {
        if (LineSeparator == null)
            LineSeparator = System.getProperty(PROPERTY_LINE_SEPARATOR);
        return LineSeparator;
    }

    public static long GetNativeHeap() {
        return Gdx.app.getNativeHeap();
    }

    public static long GetNextUniqueID() {
        return ID++;
    }

    /**
     * @param percent [0.0, 100.0]
     * @param value *
     *
     * @return *
     */
    public static float GetPercentOfFloatValue(float percent, float value) {
        return MathUtils.clamp(percent, 0.0f, 100.0f) * value / 100.0f;
    }

    public static float GetRandomFloat(float min, float max) {
        RandomXS128 random = ZPool.Obtain(RandomXS128.class);
        float randomFloat = Float.MIN_VALUE;
        if (random != null) {
            if (min > max) {
                float tmp = min;
                min = max;
                max = tmp;
            }
            randomFloat = random.nextFloat() * (max - min) + min;
            ZPool.Free(random);
        }
        return randomFloat;
    }

    public static int GetRandomInt(int min, int max) {
        RandomXS128 random = ZPool.Obtain(RandomXS128.class);
        int randomInt = Integer.MIN_VALUE;
        if (random != null) {
            if (min > max) {
                int tmp = min;
                min = max;
                max = tmp;
            }
            randomInt = random.nextInt((max - min) + 1) + min;
            ZPool.Free(random);
        }
        return randomInt;
    }

    public static int GetScreenHeight() {
        return Gdx.graphics.getHeight();
    }

    public static int GetScreenWidth() {
        return Gdx.graphics.getWidth();
    }

    public static ShapeRenderer GetShapeRenderer() {
        if (ShapeRenderer == null)
            ShapeRenderer = new ShapeRenderer();
        return ShapeRenderer;
    }

    public static SpriteBatch GetSpriteBatch() {
        if (SpriteBatch == null)
            SpriteBatch = new SpriteBatch();
        return SpriteBatch;
    }

    public static String GetTab() {
        return PROPERTY_TAB;
    }

    public static int GetVersion() {
        return Gdx.app.getVersion();
    }

    /**
     * @param r [0.0, 1.0]
     * @param g [0.0, 1.0]
     * @param b [0.0, 1.0]
     * @param a [0.0, 1.0]
     */
    public static void SetBackgroundColor(float r, float g, float b, float a) {
        BACKGROUND_COLOR.set(r, g, b, a);
    }

    public static void SetBackgroundColor(@NotNull Color color) {
        BACKGROUND_COLOR.set(color);
    }

    static {
        ZLifecycle.AddDispose(new IDispose() {
            @Override
            public void dispose() {
                BACKGROUND_COLOR.set(Color.WHITE);
                if (ShapeRenderer != null)
                    ShapeRenderer.dispose();
                if (SpriteBatch != null)
                    SpriteBatch.dispose();
                ShapeRenderer = null;
                LineSeparator = null;
                FileSeparator = null;
                SpriteBatch = null;
                Tab = null;
                ID = 0;
            }
        });
    }
}
