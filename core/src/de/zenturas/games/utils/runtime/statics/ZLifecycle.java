/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.utils.SnapshotArray;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.interfaces.IDispose;
import de.zenturas.games.utils.runtime.interfaces.IPause;
import de.zenturas.games.utils.runtime.interfaces.IRender;
import de.zenturas.games.utils.runtime.interfaces.IResize;
import de.zenturas.games.utils.runtime.interfaces.IResume;
import de.zenturas.games.utils.runtime.interfaces.IUpdate;
import de.zenturas.games.utils.runtime.internal.InternalToken;

/**
 * @version 0.0.1.7
 */
public final class ZLifecycle {
    private static SnapshotArray<IDispose> Disposes = null;
    private static SnapshotArray<IPause> Pauses = null;
    private static SnapshotArray<IRender> Renders = null;
    private static SnapshotArray<IResize> Resizes = null;
    private static SnapshotArray<IResume> Resumes = null;
    private static SnapshotArray<IUpdate> Updates = null;

    private ZLifecycle() {
    }

    public static void AddDispose(@NotNull IDispose dispose) {
        if (Disposes == null)
            Disposes = new SnapshotArray<IDispose>();
        if (!Disposes.contains(dispose, true))
            Disposes.add(dispose);
    }

    public static void AddPause(@NotNull IPause pause) {
        if (Pauses == null)
            Pauses = new SnapshotArray<IPause>();
        if (!Pauses.contains(pause, true))
            Pauses.add(pause);
    }

    public static void AddRender(@NotNull IRender render) {
        if (Renders == null)
            Renders = new SnapshotArray<IRender>();
        if (!Renders.contains(render, true))
            Renders.add(render);
    }

    public static void AddResize(@NotNull IResize resize) {
        if (Resizes == null)
            Resizes = new SnapshotArray<IResize>();
        if (!Resizes.contains(resize, true))
            Resizes.add(resize);
    }

    public static void AddResume(@NotNull IResume resume) {
        if (Resumes == null)
            Resumes = new SnapshotArray<IResume>();
        if (!Resumes.contains(resume, true))
            Resumes.add(resume);
    }

    public static void AddUpdate(@NotNull IUpdate update) {
        if (Updates == null)
            Updates = new SnapshotArray<IUpdate>();
        if (!Updates.contains(update, true))
            Updates.add(update);
    }

    public static void InternalDispose(@NotNull InternalToken token) {
        token.isAllowedToUse();
        if (Disposes != null) {
            Disposes.begin();
            for (IDispose dispose : Disposes)
                dispose.dispose();
            Disposes.end();
            Disposes.clear();
        }
        if (Renders != null)
            Renders.clear();
        if (Resizes != null)
            Resizes.clear();
        if (Resumes != null)
            Resumes.clear();
        if (Updates != null)
            Updates.clear();
        if (Pauses != null)
            Pauses.clear();
        Disposes = null;
        Renders = null;
        Resizes = null;
        Resumes = null;
        Updates = null;
        Pauses = null;
    }

    public static void InternalPause(@NotNull InternalToken token) {
        token.isAllowedToUse();
        if (Pauses != null) {
            Pauses.begin();
            for (IPause pause : Pauses)
                pause.pause();
            Pauses.end();
        }
    }

    public static void InternalRender(@NotNull InternalToken token) {
        token.isAllowedToUse();
        if (Renders != null) {
            Renders.begin();
            for (IRender render : Renders)
                render.render();
            Renders.end();
        }
    }

    public static void InternalResize(@NotNull InternalToken token, int width, int height) {
        token.isAllowedToUse();
        if (Resizes != null) {
            Resizes.begin();
            for (IResize resize : Resizes)
                resize.resize(width, height);
            Resizes.end();
        }
    }

    public static void InternalResume(@NotNull InternalToken token) {
        token.isAllowedToUse();
        if (Resumes != null) {
            Resumes.begin();
            for (IResume resume : Resumes)
                resume.resume();
            Resumes.end();
        }
    }

    public static void InternalUpdate(@NotNull InternalToken token, float delta) {
        token.isAllowedToUse();
        if (Updates != null) {
            Updates.begin();
            for (IUpdate update : Updates)
                update.update(delta);
            Updates.end();
        }
    }

    public static boolean RemoveDispose(@NotNull IDispose dispose) {
        return Disposes != null && Disposes.removeValue(dispose, true);
    }

    public static boolean RemovePause(@NotNull IPause pause) {
        return Pauses != null && Pauses.removeValue(pause, true);
    }

    public static boolean RemoveRender(@NotNull IRender render) {
        return Renders != null && Renders.removeValue(render, true);
    }

    public static boolean RemoveResize(@NotNull IResize resize) {
        return Resizes != null && Resizes.removeValue(resize, true);
    }

    public static boolean RemoveResume(@NotNull IResume resume) {
        return Resumes != null && Resumes.removeValue(resume, true);
    }

    public static boolean RemoveUpdate(@NotNull IUpdate update) {
        return Updates != null && Updates.removeValue(update, true);
    }
}
