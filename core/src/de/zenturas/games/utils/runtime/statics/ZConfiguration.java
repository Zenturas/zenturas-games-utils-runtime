/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Files.FileType;

import de.zenturas.games.utils.runtime.interfaces.IDispose;

/**
 * @version 0.0.0.0
 */
public final class ZConfiguration {
    public static final int MIN_VIRTUAL_LANDSCAPE_HEIGHT = 540;
    public static final int MIN_VIRTUAL_LANDSCAPE_WIDTH = 960;
    public static final int MIN_VIRTUAL_PORTRAIT_HEIGHT = 960;
    public static final int MIN_VIRTUAL_PORTRAIT_WIDTH = 540;
    public static final String PREFERENCES_FILE = "ZenturasGames.preferences";
    public static final FileType PREFERENCES_FILE_TYPE = FileType.Local;
    public static final String PREFERENCES_PATH = "ZenturasGames";
    public static final String TAG = "ZenturasGames";
    public static final int VIRTUAL_LANDSCAPE_HEIGHT = 1080;
    public static final int VIRTUAL_LANDSCAPE_WIDTH = 1920;
    public static final int VIRTUAL_PORTRAIT_HEIGHT = 1920;
    public static final int VIRTUAL_PORTRAIT_WIDTH = 1080;
    public static boolean UseDebug2D = false;
    public static boolean UseFPS = false;
    public static boolean UseLog = false;
    public static boolean UseMusic = false;
    public static boolean UseSound = false;
    public static boolean UseVibration = false;

    private ZConfiguration() {
    }

    static {
        ZLifecycle.AddDispose(new IDispose() {
            @Override
            public void dispose() {
                UseVibration = false;
                UseDebug2D = false;
                UseMusic = false;
                UseSound = false;
                UseFPS = false;
                UseLog = false;
            }
        });
    }
}
