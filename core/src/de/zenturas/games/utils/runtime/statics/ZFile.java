/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.anotations.Nullable;
import de.zenturas.games.utils.runtime.enums.ZWriteFileType;

/**
 * @version 0.0.0.0
 */
public final class ZFile {
    private ZFile() {
    }

    public static boolean IsExternalStorageAvailable() {
        return Gdx.files.isExternalStorageAvailable();
    }

    public static boolean IsLocalStorageAvailable() {
        return Gdx.files.isLocalStorageAvailable();
    }

    public static void WriteToFile(@NotNull String file, @NotNull String message, boolean nextLine, @NotNull ZWriteFileType type, boolean append) {
        FileHandle handle = GetFile(file, type);
        if (handle != null) {
            String combinedMessage = null;
            if (nextLine) {
                StringBuilder builder = ZPool.Obtain(StringBuilder.class);
                if (builder != null) {
                    builder.setLength(0);
                    builder.append(ZUtils.GetLineSeparator());
                    builder.append(message);
                    combinedMessage = builder.toString();
                    ZPool.Free(builder);
                }
            }
            try {
                handle.writeString(combinedMessage != null ? combinedMessage : message, append);
            } catch (GdxRuntimeException exception) {
                ZLog.LogExceptionStackTrace(exception);
            }
        }
    }

    @Nullable
    private static FileHandle GetFile(@NotNull String fileOrDir, @NotNull ZWriteFileType type) {
        switch (type) {
            case EXTERNAL:
                if (IsExternalStorageAvailable())
                    return Gdx.files.external(fileOrDir);
                return null;
            case LOCAL:
                if (IsLocalStorageAvailable())
                    return Gdx.files.local(fileOrDir);
                return null;
            case NONE:
                return null;
        }
        return null;
    }
}
