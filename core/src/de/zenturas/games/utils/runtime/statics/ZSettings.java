/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Gdx;

import de.zenturas.games.utils.runtime.anotations.NotNull;

/**
 * @version 0.0.3.4
 */
public final class ZSettings {
    private static final String MUSIC_TAG = "Music";
    private static final String SOUND_TAG = "Sound";
    private static final String VIBRATION_TAG = "Vibration";

    private ZSettings() {
    }

    /**
     * <span style="color:red">Only Android</span><br>
     * <span style="color:red">On Desktop: Returns true</span>
     *
     * @return *
     */
    public static boolean IsBackButtonActive() {
        return !Gdx.input.isCatchBackKey();
    }

    public static boolean IsMusicEnabled() {
        return ZConfiguration.UseMusic && ZPreferences.GetBoolean(MUSIC_TAG, false);
    }

    public static boolean IsSoundEnabled() {
        return ZConfiguration.UseSound && ZPreferences.GetBoolean(SOUND_TAG, false);
    }

    /**
     * <span style="color:red">Only Android</span><br>
     * <span style="color:red">On Desktop: Returns false</span>
     *
     * @return *
     */
    public static boolean IsVibrationEnabled() {
        return ZConfiguration.UseVibration && ZInput.IsVibratorAvailable() && ZPreferences.GetBoolean(VIBRATION_TAG, false);
    }

    /**
     * <span style="color:red">Only Android</span><br>
     * <span style="color:red">On Desktop: Does nothing</span>
     *
     * @param active *
     */
    public static void SetBackButtonActive(boolean active) {
        Gdx.input.setCatchBackKey(!active);
    }

    public static void SetMusicEnabled(boolean enabled) {
        ZPreferences.AddBoolean(MUSIC_TAG, enabled);
    }

    public static void SetSoundEnabled(boolean enabled) {
        ZPreferences.AddBoolean(SOUND_TAG, enabled);
    }

    /**
     * <span style="color:red">Only Desktop</span><br>
     * <span style="color:red">On Android: Does nothing</span>
     *
     * @param title *
     */
    public static void SetTitle(@NotNull String title) {
        Gdx.graphics.setTitle(title);
    }

    /**
     * <span style="color:red">Only Android</span><br>
     * <span style="color:red">On Desktop: Does nothing</span>
     *
     * @param enabled *
     */
    public static void SetVibrationEnabled(boolean enabled) {
        if (ZInput.IsVibratorAvailable())
            ZPreferences.AddBoolean(VIBRATION_TAG, enabled);
    }
}
