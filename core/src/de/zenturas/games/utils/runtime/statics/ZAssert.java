/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import de.zenturas.games.utils.runtime.anotations.Nullable;

/**
 * @version 0.0.0.0
 */
public final class ZAssert {
    private ZAssert() {
    }

    public static void AssertEquals(@Nullable String expected, @Nullable String actual) {
        if (expected != null && actual != null && !expected.equals(actual))
            ZLog.LogExceptionStackTrace(new RuntimeException());
        else if ((expected == null && actual != null) || (expected != null && actual == null))
            ZLog.LogExceptionStackTrace(new RuntimeException());
    }

    public static void AssertFalse(boolean value) {
        if (value)
            ZLog.LogExceptionStackTrace(new RuntimeException());
    }

    public static void AssertNotEquals(@Nullable String expected, @Nullable String actual) {
        if (expected != null && actual != null && expected.equals(actual))
            ZLog.LogExceptionStackTrace(new RuntimeException());
        else if (expected == null && actual == null)
            ZLog.LogExceptionStackTrace(new RuntimeException());
    }

    public static void AssertNotNull(@Nullable Object object) {
        if (object == null)
            ZLog.LogExceptionStackTrace(new RuntimeException());
    }

    public static void AssertNull(@Nullable Object object) {
        if (object != null)
            ZLog.LogExceptionStackTrace(new RuntimeException());
    }

    public static void AssertTrue(boolean value) {
        if (!value)
            ZLog.LogExceptionStackTrace(new RuntimeException());
    }
}
