/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Peripheral;

/**
 * @version 0.0.3.4
 */
public final class ZInput {
    private ZInput() {
    }

    /**
     * <span style="color:red">Only Android</span><br>
     * <span style="color:red">On Desktop: Returns false</span>
     *
     * @return *
     */
    public static boolean IsVibratorAvailable() {
        return Gdx.input.isPeripheralAvailable(Peripheral.Vibrator);
    }

    /**
     * <span style="color:red">Only Android</span><br>
     * <span style="color:red">On Desktop: Does nothing</span>
     *
     * @param visible *
     */
    public static void SetOnScreenKeyboardVisible(boolean visible) {
        Gdx.input.setOnscreenKeyboardVisible(visible);
    }
}
