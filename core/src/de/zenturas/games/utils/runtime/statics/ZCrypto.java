/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.utils.Base64Coder;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.anotations.Nullable;

/**
 * @version 0.0.0.0
 */
public final class ZCrypto {
    private ZCrypto() {
    }

    @Nullable
    public static String Base64Decode(@NotNull String encodedString) {
        try {
            return Base64Coder.decodeString(encodedString);
        } catch (IllegalArgumentException exception) {
            ZLog.LogExceptionStackTrace(exception);
            return null;
        }
    }

    public static String Base64Encode(@NotNull String string) {
        return Base64Coder.encodeString(string);
    }
}
