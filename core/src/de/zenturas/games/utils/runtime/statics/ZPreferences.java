/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.interfaces.IDispose;

/**
 * @version 0.0.0.0
 */
public final class ZPreferences {
    private static Preferences Preferences = null;

    private ZPreferences() {
    }

    public static void AddBoolean(@NotNull String key, boolean value) {
        Preferences.putBoolean(key, value);
        Preferences.flush();
    }

    public static void Clear() {
        Preferences.clear();
        Preferences.flush();
    }

    public static boolean GetBoolean(@NotNull String key, boolean defaultValue) {
        return Preferences.getBoolean(key, defaultValue);
    }

    static {
        Preferences = Gdx.app.getPreferences(ZConfiguration.PREFERENCES_FILE);
        ZLifecycle.AddDispose(new IDispose() {
            @Override
            public void dispose() {
                Preferences = null;
            }
        });
    }
}
