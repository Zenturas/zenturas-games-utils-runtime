/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.statics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.GLVersion;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.TimeUtils;

import java.io.PrintWriter;
import java.io.StringWriter;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.enums.ZLogType;
import de.zenturas.games.utils.runtime.enums.ZWriteFileType;
import de.zenturas.games.utils.runtime.interfaces.IDispose;
import de.zenturas.games.utils.runtime.interfaces.IUpdate;

/**
 * @version 0.0.5.7
 */
public final class ZLog {
    private static final float DEBUG_PRINT_PERIOD = 5.0f;
    private static final String LOG_COLON = ": ";
    private static final String LOG_ERROR = "[ERROR] ";
    private static final String LOG_FILE_NAME = "ZenturasGamesUtilsRuntime.log";
    private static final String LOG_INFO = "[INFO] ";
    private static String LogFilePath = null;
    private static long StartTime = Long.MIN_VALUE;
    private static float Timer = 0.0f;

    private ZLog() {
    }

    public static String ErrorStackTraceToString(@NotNull Error error) {
        StringWriter writer = new StringWriter();
        error.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

    public static String ExceptionStackTraceToString(@NotNull Exception exception) {
        StringWriter writer = new StringWriter();
        exception.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

    public static void Log(char value) {
        LogObject(value, ZLogType.INFO);
    }

    public static void Log(float value) {
        LogObject(value, ZLogType.INFO);
    }

    public static void Log(int value) {
        LogObject(value, ZLogType.INFO);
    }

    public static void Log(long value) {
        LogObject(value, ZLogType.INFO);
    }

    public static void Log(boolean value) {
        LogObject(value, ZLogType.INFO);
    }

    public static void Log(@NotNull String value) {
        LogObject(value, ZLogType.INFO);
    }

    public static void LogErrorStackTrace(@NotNull Error error) {
        LogObject(ErrorStackTraceToString(error), ZLogType.ERROR);
    }

    public static void LogExceptionStackTrace(@NotNull Exception exception) {
        LogObject(ExceptionStackTraceToString(exception), ZLogType.ERROR);
    }

    private static String GetLogFilePath() {
        if (LogFilePath == null)
            LogFilePath = ZConfiguration.PREFERENCES_PATH + ZUtils.GetFileSeparator() + LOG_FILE_NAME;
        return LogFilePath;
    }

    private static void LogDebug() {
        StringBuilder builder = ZPool.Obtain(StringBuilder.class);
        if (builder != null) {
            GLVersion version = ZUtils.GetGLVersion();
            String line = ZUtils.GetLineSeparator();
            String tab = ZUtils.GetTab();
            builder.setLength(0);
            builder.append(line);
            builder.append(tab);
            builder.append("Release Version: ");
            builder.append(version.getReleaseVersion());
            builder.append(line);
            builder.append(tab);
            builder.append("Renderer String: ");
            builder.append(version.getRendererString());
            builder.append(line);
            builder.append(tab);
            builder.append("Major Version: ");
            builder.append(version.getMajorVersion());
            builder.append(line);
            builder.append(tab);
            builder.append("Minor Version: ");
            builder.append(version.getMinorVersion());
            builder.append(line);
            builder.append(tab);
            builder.append("Vendor Version: ");
            builder.append(version.getVendorString());
            builder.append(line);
            builder.append(tab);
            builder.append("Native Heap: ");
            builder.append(ZUtils.GetNativeHeap());
            builder.append(line);
            builder.append(tab);
            builder.append("Jave Heap: ");
            builder.append(ZUtils.GetJavaHeap());
            builder.append(line);
            builder.append(tab);
            builder.append("Software Version: ");
            builder.append(ZUtils.GetVersion());
            LogObject(builder.toString(), ZLogType.INFO);
            ZPool.Free(builder);
        }
    }

    private static void LogFPS() {
        if (StartTime == Long.MIN_VALUE)
            StartTime = TimeUtils.nanoTime();
        if (TimeUtils.nanoTime() - StartTime > 1000000000L) {
            LogObject(Gdx.graphics.getFramesPerSecond(), ZLogType.INFO);
            StartTime = TimeUtils.nanoTime();
        }
    }

    private static void LogObject(@NotNull Object object, @NotNull ZLogType type) {
        StringBuilder builder = ZPool.Obtain(StringBuilder.class);
        if (builder != null) {
            builder.setLength(0);
            builder.append(type == ZLogType.ERROR ? LOG_ERROR : LOG_INFO);
            builder.append(ZUtils.GetCurrentDate());
            builder.append(LOG_COLON);
            builder.append(object);
            String text = builder.toString();
            ZFile.WriteToFile(GetLogFilePath(), text, true, ZFile.IsExternalStorageAvailable() ? ZWriteFileType.EXTERNAL : ZFile.IsLocalStorageAvailable() ? ZWriteFileType.LOCAL : ZWriteFileType.NONE, true);
            if (ZConfiguration.UseLog)
                System.out.println(text);
            ZPool.Free(builder);
        }
    }

    static {
        ZLifecycle.AddDispose(new IDispose() {
            @Override
            public void dispose() {
                StartTime = Long.MIN_VALUE;
                LogFilePath = null;
                Timer = 0.0f;
            }
        });
        ZLifecycle.AddUpdate(new IUpdate() {
            @Override
            public void update(float delta) {
                if (ZConfiguration.UseLog) {
                    Timer += delta;
                    if (Timer >= DEBUG_PRINT_PERIOD) {
                        Timer -= DEBUG_PRINT_PERIOD;
                        if (ZConfiguration.UseFPS)
                            LogFPS();
                        LogDebug();
                    }
                }
            }
        });
    }
}
