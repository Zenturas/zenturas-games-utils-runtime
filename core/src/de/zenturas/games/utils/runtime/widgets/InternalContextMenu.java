/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.widgets;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.anotations.Nullable;
import de.zenturas.games.utils.runtime.helpers.ZPadSize;
import de.zenturas.games.utils.runtime.styles.ZContextMenuStyle;

/**
 * @version 0.0.2.0
 */
abstract class InternalContextMenu {
    InternalTable contextMenu;
    boolean isDirty;
    private ZContextSubMenu currentOpenedContextSubMenu;
    private Array<ZMenuItem> items;
    private ZPadSize oldItemsPadSize;
    private ZPadSize oldMenuPadSize;

    public InternalContextMenu(@NotNull ZContextMenuStyle style) {
        currentOpenedContextSubMenu = null;
        contextMenu = new InternalTable();
        contextMenu.setBackground(style.background);
        contextMenu.defaults().expandX().fillX();
        oldItemsPadSize = getItemsSize();
        oldMenuPadSize = getMenuSize();
        items = new Array<ZMenuItem>();
        isDirty = true;
    }

    public void addItem(@NotNull ZMenuItem item) {
        contextMenu.add(item.menuItem).row();
        item.contextMenuParent = this;
        items.add(item);
        isDirty = true;
    }

    public void resetPad() {
        ZPadSize itemsPad = getItemsSize();
        ZPadSize menuPad = getMenuSize();
        if (!itemsPad.isEqual(oldItemsPadSize))
            setItemsPad(oldItemsPadSize);
        if (!menuPad.isEqual(oldMenuPadSize))
            setMenuPad(oldMenuPadSize);
    }

    public void setItemsPadSize(@NotNull ZPadSize size) {
        setItemsPad(size);
    }

    public void setMenuPadSize(@NotNull ZPadSize size) {
        setMenuPad(size);
    }

    boolean insideAllContext(float x, float y) {
        float menuX = contextMenu.getX();
        float menuY = contextMenu.getY();
        return menuX <= x && menuX + contextMenu.getWidth() >= x && menuY <= y && menuY + contextMenu.getHeight() >= y || // Inside context menu
                currentOpenedContextSubMenu != null && currentOpenedContextSubMenu.insideAllContext(x, y); // Inside all context sub menu
    }

    void setCurrentOpenedContextSubMenu(@Nullable ZContextSubMenu currentOpenedContextSubMenu) {
        if (this.currentOpenedContextSubMenu != currentOpenedContextSubMenu) {
            if (this.currentOpenedContextSubMenu != null)
                this.currentOpenedContextSubMenu.contextMenu.remove();
            this.currentOpenedContextSubMenu = currentOpenedContextSubMenu;
        }
    }

    void update() {
        if (isDirty) {
            contextMenu.invalidateHierarchy();
            contextMenu.pack();
            float width = getWidth();
            contextMenu.defaults().width(width);
            for (Cell cell : contextMenu.getCells())
                cell.width(width);
            contextMenu.invalidateHierarchy();
            contextMenu.pack();
            isDirty = false;
        }
    }

    private ZPadSize getItemsSize() {
        ZPadSize padSize = new ZPadSize();
        Cell defaults = contextMenu.defaults();
        padSize.bottom = defaults.getPadBottom();
        padSize.right = defaults.getPadRight();
        padSize.left = defaults.getPadLeft();
        padSize.top = defaults.getPadTop();
        return padSize;
    }

    private ZPadSize getMenuSize() {
        ZPadSize padSize = new ZPadSize();
        padSize.bottom = contextMenu.getPadBottom();
        padSize.right = contextMenu.getPadRight();
        padSize.left = contextMenu.getPadLeft();
        padSize.top = contextMenu.getPadTop();
        return padSize;
    }

    private float getWidth() {
        float maxShortcutWidth = 0.0f;
        float maxItemIconWidth = 0.0f;
        float maxSubIconWidth = 0.0f;
        float maxTitleWidth = 0.0f;
        float maxPadWidth = 0.0f;
        for (ZMenuItem item : items) {
            Image itemIcon = item.itemIconImage;
            Image subIcon = item.subIconImage;
            maxItemIconWidth = Math.max(maxItemIconWidth, itemIcon == null ? 0.0f : itemIcon.getWidth());
            maxSubIconWidth = Math.max(maxSubIconWidth, subIcon == null ? 0.0f : subIcon.getWidth());
            maxShortcutWidth = Math.max(maxShortcutWidth, item.shortcutLabel.getWidth());
            maxTitleWidth = Math.max(maxTitleWidth, item.titleLabel.getWidth());
            maxPadWidth = Math.max(maxPadWidth, item.padWidth);
        }
        for (ZMenuItem item : items) {
            item.itemIconCell.width(maxItemIconWidth);
            item.subIconCell.width(maxSubIconWidth);
        }
        Cell defaults = contextMenu.defaults();
        return maxShortcutWidth + maxTitleWidth + maxSubIconWidth + maxPadWidth + maxItemIconWidth + contextMenu.getPadLeft() + contextMenu.getPadRight() + defaults.getPadLeft() + defaults.getPadRight();
    }

    private void setItemsPad(@NotNull ZPadSize size) {
        Cell defaults = contextMenu.defaults();
        defaults.padBottom(size.bottom);
        defaults.padRight(size.right);
        defaults.padLeft(size.left);
        defaults.padTop(size.top);
        for (Cell cell : contextMenu.getCells()) {
            cell.padBottom(size.bottom);
            cell.padRight(size.right);
            cell.padLeft(size.left);
            cell.padTop(size.top);
        }
        isDirty = true;
    }

    private void setMenuPad(@NotNull ZPadSize size) {
        contextMenu.padBottom(size.bottom);
        contextMenu.padRight(size.right);
        contextMenu.padLeft(size.left);
        contextMenu.padTop(size.top);
        isDirty = true;
    }

    abstract void remove();
    abstract void setStage();

    class InternalTable extends Table {
        @Override
        public void act(float delta) {
            super.act(delta);
            update();
        }

        @Override
        public boolean remove() {
            setCurrentOpenedContextSubMenu(null);
            InternalContextMenu.this.remove();
            return super.remove();
        }

        @Override
        protected void setStage(Stage stage) {
            super.setStage(stage);
            InternalContextMenu.this.setStage();
        }
    }
}
