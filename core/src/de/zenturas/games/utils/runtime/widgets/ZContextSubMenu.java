/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.styles.ZContextMenuStyle;

/**
 * @version 0.0.2.0
 */
public class ZContextSubMenu extends InternalContextMenu {
    public ZContextSubMenu(@NotNull ZContextMenuStyle style) {
        super(style);
    }

    public ZContextSubMenu(@NotNull Skin skin) {
        this(skin.get(ZContextMenuStyle.class));
    }

    public ZContextSubMenu(@NotNull Skin skin, @NotNull String styleName) {
        this(skin.get(styleName, ZContextMenuStyle.class));
    }

    @Override
    void remove() {
    }

    @Override
    void setStage() {
    }
}