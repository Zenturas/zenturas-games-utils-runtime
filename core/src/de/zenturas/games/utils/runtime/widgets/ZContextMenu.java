/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.widgets;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.interfaces.IDispose;
import de.zenturas.games.utils.runtime.interfaces.IResize;
import de.zenturas.games.utils.runtime.scene2D.ZStage2D;
import de.zenturas.games.utils.runtime.statics.ZLifecycle;
import de.zenturas.games.utils.runtime.styles.ZContextMenuStyle;

/**
 * @version 0.0.4.7
 */
public class ZContextMenu extends InternalContextMenu {
    static ZContextMenu CurrentOpenedContextMenu = null;
    private InputListener removeListener;

    public ZContextMenu(@NotNull ZContextMenuStyle style) {
        super(style);
        initListener();
    }

    public ZContextMenu(@NotNull Skin skin) {
        this(skin.get(ZContextMenuStyle.class));
    }

    public ZContextMenu(@NotNull Skin skin, @NotNull String styleName) {
        this(skin.get(styleName, ZContextMenuStyle.class));
    }

    public void show(@NotNull ZStage2D stage, float x, float y) {
        if (contextMenu.getChildren().size > 0 && CurrentOpenedContextMenu == null) {
            update();
            CurrentOpenedContextMenu = this;
            float viewportHeight = stage.getViewportHeight();
            float contextHeight = contextMenu.getHeight();
            float contextWidth = contextMenu.getWidth();
            float viewportY = stage.getViewportY();
            float viewportX = stage.getViewportX();
            float viewportYMinusY = viewportY - y;
            float viewportXMinusX = viewportX - x;
            boolean onRightEnoughSpace = stage.getViewportWidth() + viewportXMinusX >= contextWidth; // If not enough space then draw on left side
            boolean onTopEnoughSpace = viewportHeight + viewportYMinusY >= contextHeight; // If not enough space then draw misplaced to the bottom
            boolean onBottomEnoughSpace = -1.0f * viewportYMinusY >= contextHeight; // If not enough space then draw on top side
            boolean onLeftEnoughSpace = -1.0f * viewportXMinusX >= contextWidth; // If not enough space then draw misplaced to the right
            contextMenu.setPosition(onRightEnoughSpace ? x : onLeftEnoughSpace ? x - contextWidth : viewportX + 5.0f, //
                    onBottomEnoughSpace ? y - contextHeight : onTopEnoughSpace ? y : viewportHeight + viewportY - contextHeight - 5.0f);
            stage.addActor(contextMenu);
        }
    }

    @Override
    void remove() {
        Stage stage = contextMenu.getStage();
        if (stage != null)
            stage.removeListener(removeListener);
        CurrentOpenedContextMenu = null;
    }

    @Override
    void setStage() {
        Stage stage = contextMenu.getStage();
        if (stage != null) {
            Array<EventListener> listeners = stage.getRoot().getListeners();
            if (!listeners.contains(removeListener, true))
                listeners.insert(0, removeListener);
        }
    }

    private void initListener() {
        removeListener = new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode) {
                    case Keys.ESCAPE:
                    case Keys.BACK:
                        contextMenu.remove();
                        return true;
                }
                return false;
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!insideAllContext(x, y))
                    contextMenu.remove();
                return false;
            }
        };
    }

    static {
        ZLifecycle.AddDispose(new IDispose() {
            @Override
            public void dispose() {
                CurrentOpenedContextMenu = null;
            }
        });
        ZLifecycle.AddResize(new IResize() {
            @Override
            public void resize(int width, int height) {
                if (CurrentOpenedContextMenu != null)
                    CurrentOpenedContextMenu.contextMenu.remove();
            }
        });
    }
}
