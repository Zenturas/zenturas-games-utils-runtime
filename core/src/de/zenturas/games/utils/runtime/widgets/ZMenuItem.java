/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.widgets;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.anotations.Nullable;
import de.zenturas.games.utils.runtime.helpers.ZPadSize;
import de.zenturas.games.utils.runtime.interfaces.ICallback;
import de.zenturas.games.utils.runtime.statics.ZPool;
import de.zenturas.games.utils.runtime.styles.ZMenuItemStyle;

/**
 * @version 0.0.2.2
 */
public class ZMenuItem {
    private static final float PAD_ICON_RIGHT = 2.0f;
    private static final float PAD_SHORTCUT_RIGHT = 2.0f;
    private static final float PAD_TITLE_RIGHT = 20.0f;
    InternalContextMenu contextMenuParent;
    Cell<Image> itemIconCell;
    Image itemIconImage;
    Button menuItem;
    float padWidth;
    Label shortcutLabel;
    Cell<Image> subIconCell;
    Image subIconImage;
    Label titleLabel;
    private Array<ICallback> clickCallbacks;
    private ZContextSubMenu contextSubMenuChild;
    private ZPadSize oldPadSize;

    public ZMenuItem(@NotNull String title, @NotNull ZMenuItemStyle style) {
        shortcutLabel = new Label("", style.shortcutStyle);
        shortcutLabel.setTouchable(Touchable.disabled);
        titleLabel = new Label(title, style.titleStyle);
        titleLabel.setTouchable(Touchable.disabled);
        menuItem = new Button(style.itemButtonStyle);
        menuItem.defaults().uniformY().pad(0.0f).expandY();
        padWidth = Float.MIN_VALUE;
        contextSubMenuChild = null;
        contextMenuParent = null;
        clickCallbacks = null;
        itemIconImage = null;
        subIconImage = null;
        initMenuItem(style);
        setPadSize(null);
        initListener();
    }

    public ZMenuItem(@NotNull String title, @NotNull Skin skin, @NotNull String styleName) {
        this(title, skin.get(styleName, ZMenuItemStyle.class));
    }

    public ZMenuItem(@NotNull String title, @NotNull Skin skin) {
        this(title, skin.get(ZMenuItemStyle.class));
    }

    public boolean addListener(@NotNull ICallback clickEvent) {
        if (clickCallbacks == null)
            clickCallbacks = new Array<ICallback>();
        if (!clickCallbacks.contains(clickEvent, true)) {
            clickCallbacks.add(clickEvent);
            return true;
        }
        return false;
    }

    public String getKeycode() {
        return shortcutLabel.getText().toString();
    }

    public String getTitle() {
        return titleLabel.getText().toString();
    }

    public boolean isDisabled() {
        return menuItem.isDisabled();
    }

    public void setDisabled(boolean isDisabled) {
        menuItem.setTouchable(isDisabled ? Touchable.disabled : Touchable.enabled);
        menuItem.setDisabled(isDisabled);
    }

    public void setKeycode(@Nullable String keycode) {
        shortcutLabel.setText(keycode == null ? "" : keycode);
        if (contextMenuParent != null)
            contextMenuParent.isDirty = true;
    }

    public void setPadSize(@Nullable ZPadSize size) {
        padWidth = 0.0f;
        if (size != null) {
            menuItem.padBottom(size.bottom);
            menuItem.padRight(size.right);
            menuItem.padLeft(size.left);
            menuItem.padTop(size.top);
            padWidth += size.right;
            padWidth += size.left;
        }
        if (padWidth <= 0.0f) {
            menuItem.padBottom(oldPadSize.bottom);
            menuItem.padRight(oldPadSize.right);
            menuItem.padLeft(oldPadSize.left);
            menuItem.padTop(oldPadSize.top);
            padWidth += oldPadSize.right;
            padWidth += oldPadSize.left;
        }
        padWidth += PAD_SHORTCUT_RIGHT;
        padWidth += PAD_TITLE_RIGHT;
        padWidth += PAD_ICON_RIGHT;
        if (contextMenuParent != null)
            contextMenuParent.isDirty = true;
    }

    public void setSubMenu(@Nullable ZContextSubMenu subMenu) {
        subIconCell.setActor(subMenu == null ? null : subIconImage);
        if (contextSubMenuChild != null)
            contextSubMenuChild.contextMenu.remove();
        contextSubMenuChild = subMenu;
        if (contextMenuParent != null)
            contextMenuParent.isDirty = true;
    }

    public void setTitle(@NotNull String title) {
        titleLabel.setText(title);
        if (contextMenuParent != null)
            contextMenuParent.isDirty = true;
    }

    private void initListener() {
        menuItem.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (clickCallbacks != null)
                    for (ICallback callback : clickCallbacks)
                        callback.callback();
                if (contextSubMenuChild == null) {
                    ZContextMenu currentMenu = ZContextMenu.CurrentOpenedContextMenu;
                    if (currentMenu != null)
                        currentMenu.contextMenu.remove();
                }
            }
        });
        menuItem.addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (contextMenuParent != null) {
                    contextMenuParent.setCurrentOpenedContextSubMenu(null);
                    if (contextSubMenuChild != null && !menuItem.isDisabled()) {
                        InternalContextMenu.InternalTable subTable = contextSubMenuChild.contextMenu;
                        if (subTable.getChildren().size > 0) {
                            contextSubMenuChild.update();
                            Vector2 position = ZPool.Obtain(Vector2.class);
                            if (position != null) {
                                position.set(0.0f, 0.0f);
                                menuItem.localToStageCoordinates(position);
                                float itemWidth = menuItem.getWidth();
                                Stage stage = menuItem.getStage();
                                Camera camera = stage.getCamera();
                                float camHeight = camera.viewportHeight;
                                float camWidth = camera.viewportWidth;
                                float camY = camera.position.y - camHeight * 0.5f;
                                float camX = camera.position.x - camWidth * 0.5f;
                                float positionX = position.x;
                                float positionY = position.y;
                                float subHeight = subTable.getHeight();
                                float subWidth = subTable.getWidth();
                                float positionYPlusItemHeight = positionY + menuItem.getHeight();
                                boolean onRightEnoughSpace = camWidth - positionX - itemWidth + camX >= subWidth; // If not enough space then draw on left side
                                boolean onBottomEnoughSpace = positionYPlusItemHeight - camY >= subHeight; // If not enough space then draw on top side
                                boolean onTopEnoughSpace = camHeight - positionY + camY >= subHeight; // If not enough space then draw misplaced to the bottom
                                boolean onLeftEnoughSpace = positionX - camX >= subWidth; // If not enough space then draw misplaced to the right
                                subTable.setPosition(onRightEnoughSpace ? positionX + itemWidth : onLeftEnoughSpace ? positionX - subWidth : camX + 5.0f, //
                                        onBottomEnoughSpace ? -subHeight + positionYPlusItemHeight : onTopEnoughSpace ? positionY : camHeight + camY - subHeight - 5.0f);
                                contextMenuParent.setCurrentOpenedContextSubMenu(contextSubMenuChild);
                                stage.addActor(subTable);
                                ZPool.Free(position);
                            }
                        }
                    }
                }
            }
        });
    }

    private void initMenuItem(@NotNull ZMenuItemStyle style) {
        Drawable itemIcon = style.itemIcon;
        Drawable subIcon = style.subIcon;
        if (itemIcon != null) {
            itemIconImage = new Image(itemIcon);
            itemIconImage.setTouchable(Touchable.disabled);
        }
        itemIconCell = menuItem.add(itemIconImage).center().padRight(PAD_ICON_RIGHT);
        menuItem.add(titleLabel).expandX().center().left().padRight(PAD_TITLE_RIGHT);
        menuItem.add(shortcutLabel).padRight(PAD_SHORTCUT_RIGHT);
        subIconCell = menuItem.add(subIconImage).center();
        if (subIcon != null) {
            subIconImage = new Image(subIcon);
            subIconImage.setTouchable(Touchable.disabled);
        }
        oldPadSize = new ZPadSize();
        oldPadSize.bottom = menuItem.getPadBottom();
        oldPadSize.right = menuItem.getPadRight();
        oldPadSize.left = menuItem.getPadLeft();
        oldPadSize.top = menuItem.getPadTop();
    }
}
