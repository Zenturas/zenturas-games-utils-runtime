/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.manager;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.I18NBundleLoader;
import com.badlogic.gdx.assets.loaders.I18NBundleLoader.I18NBundleParameter;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader.SkinParameter;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.Locale;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.anotations.Nullable;
import de.zenturas.games.utils.runtime.interfaces.IDispose;
import de.zenturas.games.utils.runtime.interfaces.IUpdate;
import de.zenturas.games.utils.runtime.statics.ZLifecycle;

/**
 * @version 0.0.3.7
 */
public final class ZAssetManager {
    private static ZAssetManager AssetManager = null;
    private ObjectMap<String, I18NBundle> bundles;
    private Array<String> ids;
    private boolean isDone;
    private AssetManager manager;
    private ObjectMap<String, Music> musics;
    private Array<String> queueBundle;
    private Array<String> queueMusic;
    private Array<String> queueSkin;
    private Array<String> queueSound;
    private Array<String> queueTexture;
    private ObjectMap<String, Skin> skins;
    private ObjectMap<String, Sound> sounds;
    private ObjectMap<String, Texture> textures;

    private ZAssetManager() {
        manager = new AssetManager(new InternalFileHandleResolver(), false);
        ids = new Array<String>();
        queueTexture = null;
        queueBundle = null;
        queueSound = null;
        queueMusic = null;
        queueSkin = null;
        textures = null;
        bundles = null;
        isDone = false;
        musics = null;
        sounds = null;
        skins = null;
        initDispose();
        initUpdate();
    }

    public static ZAssetManager GetAssetManager() {
        if (AssetManager == null)
            AssetManager = new ZAssetManager();
        return AssetManager;
    }

    public void addBundle(@NotNull String file, @Nullable Locale locale, @Nullable String encoding) {
        if (!ids.contains(file, false)) {
            if (bundles == null) {
                manager.setLoader(I18NBundle.class, new I18NBundleLoader(manager.getFileHandleResolver()));
                bundles = new ObjectMap<String, I18NBundle>();
                queueBundle = new Array<String>();
            }
            manager.load(file, I18NBundle.class, new I18NBundleParameter(locale, encoding));
            queueBundle.add(file);
            ids.add(file);
            isDone = false;
        }
    }

    public void addMusic(@NotNull String file) {
        if (!ids.contains(file, false)) {
            if (musics == null) {
                manager.setLoader(Music.class, new MusicLoader(manager.getFileHandleResolver()));
                musics = new ObjectMap<String, Music>();
                queueMusic = new Array<String>();
            }
            manager.load(file, Music.class);
            queueMusic.add(file);
            ids.add(file);
            isDone = false;
        }
    }

    public void addSkin(@NotNull String file, @Nullable String textureAtlasFile) {
        if (!ids.contains(file, false)) {
            if (skins == null) {
                FileHandleResolver resolver = manager.getFileHandleResolver();
                manager.setLoader(TextureAtlas.class, new TextureAtlasLoader(resolver));
                manager.setLoader(Texture.class, new TextureLoader(resolver));
                manager.setLoader(Skin.class, new SkinLoader(resolver));
                skins = new ObjectMap<String, Skin>();
                queueSkin = new Array<String>();
            }
            manager.load(file, Skin.class, textureAtlasFile != null ? new SkinParameter(textureAtlasFile) : null);
            queueSkin.add(file);
            ids.add(file);
            isDone = false;
        }
    }

    public void addSound(@NotNull String file) {
        if (!ids.contains(file, false)) {
            if (sounds == null) {
                manager.setLoader(Sound.class, new SoundLoader(manager.getFileHandleResolver()));
                sounds = new ObjectMap<String, Sound>();
                queueSound = new Array<String>();
            }
            manager.load(file, Sound.class);
            queueSound.add(file);
            ids.add(file);
            isDone = false;
        }
    }

    public void addTexture(@NotNull String file, @Nullable TextureFilter minFilter, @Nullable TextureFilter magFilter, @Nullable Format format, boolean useMipMap, @Nullable TextureWrap wrapU, @Nullable TextureWrap wrapV) {
        if (!ids.contains(file, false)) {
            if (textures == null) {
                manager.setLoader(Texture.class, new TextureLoader(manager.getFileHandleResolver()));
                textures = new ObjectMap<String, Texture>();
                queueTexture = new Array<String>();
            }
            TextureParameter parameter = new TextureParameter();
            parameter.minFilter = minFilter == null ? TextureFilter.Linear : minFilter;
            parameter.magFilter = magFilter == null ? TextureFilter.Linear : magFilter;
            parameter.wrapU = wrapU == null ? TextureWrap.ClampToEdge : wrapU;
            parameter.wrapV = wrapV == null ? TextureWrap.ClampToEdge : wrapV;
            parameter.format = format == null ? Format.RGBA8888 : format;
            parameter.genMipMaps = useMipMap;
            manager.load(file, Texture.class, parameter);
            queueTexture.add(file);
            ids.add(file);
            isDone = false;
        }
    }

    public void clear() {
        manager.clear();
        clearArrays();
    }

    public void finishLoading() {
        manager.finishLoading();
        update(0.0f);
    }

    public void finishLoading(@NotNull String file) {
        manager.finishLoadingAsset(file);
        update(0.0f);
    }

    @Nullable
    public I18NBundle getBundle(@NotNull String file) {
        return bundles != null ? bundles.get(file) : null;
    }

    @Nullable
    public Music getMusic(@NotNull String file) {
        return musics != null ? musics.get(file) : null;
    }

    public float getProgress() {
        return manager.getProgress();
    }

    @Nullable
    public Skin getSkin(@NotNull String file) {
        return skins != null ? skins.get(file) : null;
    }

    @Nullable
    public Sound getSound(@NotNull String file) {
        return sounds != null ? sounds.get(file) : null;
    }

    @Nullable
    public Texture getTexture(@NotNull String file) {
        return textures != null ? textures.get(file) : null;
    }

    public boolean isDone() {
        return isDone;
    }

    private void clearArrays() {
        if (queueTexture != null)
            queueTexture.clear();
        if (queueBundle != null)
            queueBundle.clear();
        if (queueMusic != null)
            queueMusic.clear();
        if (queueSound != null)
            queueSound.clear();
        if (queueSkin != null)
            queueSkin.clear();
        if (textures != null)
            textures.clear();
        if (bundles != null)
            bundles.clear();
        if (musics != null)
            musics.clear();
        if (sounds != null)
            sounds.clear();
        if (skins != null)
            skins.clear();
        if (ids != null)
            ids.clear();
    }

    private void initDispose() {
        ZLifecycle.AddDispose(new IDispose() {
            @Override
            public void dispose() {
                if (AssetManager != null) {
                    AssetManager.manager.dispose();
                    AssetManager.clearArrays();
                    AssetManager = null;
                }
            }
        });
    }

    private void initUpdate() {
        ZLifecycle.AddUpdate(new IUpdate() {
            @Override
            public void update(float delta) {
                if (AssetManager != null)
                    AssetManager.update(delta);
            }
        });
    }

    private void update(float delta) {
        isDone = manager.update();
        if (queueTexture != null)
            for (int i = queueTexture.size - 1; i >= 0; i--) {
                String texture = queueTexture.get(i);
                if (manager.isLoaded(texture, Texture.class)) {
                    textures.put(texture, manager.get(texture, Texture.class));
                    queueTexture.removeValue(texture, false);
                }
            }
        if (queueBundle != null)
            for (int i = queueBundle.size - 1; i >= 0; i--) {
                String bundle = queueBundle.get(i);
                if (manager.isLoaded(bundle, I18NBundle.class)) {
                    bundles.put(bundle, manager.get(bundle, I18NBundle.class));
                    queueBundle.removeValue(bundle, false);
                }
            }
        if (queueMusic != null)
            for (int i = queueMusic.size - 1; i >= 0; i--) {
                String music = queueMusic.get(i);
                if (manager.isLoaded(music, Music.class)) {
                    musics.put(music, manager.get(music, Music.class));
                    queueMusic.removeValue(music, false);
                }
            }
        if (queueSound != null)
            for (int i = queueSound.size - 1; i >= 0; i--) {
                String sound = queueSound.get(i);
                if (manager.isLoaded(sound, Sound.class)) {
                    sounds.put(sound, manager.get(sound, Sound.class));
                    queueSound.removeValue(sound, false);
                }
            }
        if (queueSkin != null)
            for (int i = queueSkin.size - 1; i >= 0; i--) {
                String skin = queueSkin.get(i);
                if (manager.isLoaded(skin, Skin.class)) {
                    skins.put(skin, manager.get(skin, Skin.class));
                    queueSkin.removeValue(skin, false);
                }
            }
    }
}
