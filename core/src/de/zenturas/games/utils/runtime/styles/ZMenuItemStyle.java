/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.styles;

import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.anotations.Nullable;

/**
 * @version 0.0.0.0
 */
public class ZMenuItemStyle {
    public ButtonStyle itemButtonStyle;
    public Drawable itemIcon; // Optional
    public LabelStyle shortcutStyle;
    public Drawable subIcon; // Optional
    public LabelStyle titleStyle;

    public ZMenuItemStyle(@NotNull ButtonStyle itemButtonStyle, @Nullable Drawable itemIcon, @NotNull LabelStyle shortcutStyle, @Nullable Drawable subIcon, @NotNull LabelStyle titleStyle) {
        this.itemButtonStyle = itemButtonStyle;
        this.shortcutStyle = shortcutStyle;
        this.titleStyle = titleStyle;
        this.itemIcon = itemIcon;
        this.subIcon = subIcon;
    }

    public ZMenuItemStyle(@NotNull ZMenuItemStyle style) {
        itemButtonStyle = style.itemButtonStyle;
        shortcutStyle = style.shortcutStyle;
        titleStyle = style.titleStyle;
        itemIcon = style.itemIcon;
        subIcon = style.subIcon;
    }

    public ZMenuItemStyle() {
    }
}