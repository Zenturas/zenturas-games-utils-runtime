/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.entities2D;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.zenturas.games.utils.runtime.anotations.NotNull;
import de.zenturas.games.utils.runtime.helpers.Line;
import de.zenturas.games.utils.runtime.statics.ZPool;

/**
 * @version 0.0.2.0
 */
public class ZGrid extends ZAbstractEntityPS2D {
    private static final float EPSILON = 0.001f;
    private static final float LINE_SIZE = 1.0f;
    private Color firstColor;
    private boolean isDirty;
    private Array<Line> lines;
    private TextureRegion region;
    private Color secondColor;
    private float segmentSize;
    private float space;
    private Color thirdColor;

    public ZGrid(@NotNull Texture texture) {
        this(new TextureRegion(texture));
    }

    public ZGrid(@NotNull TextureRegion region) {
        secondColor = new Color(Color.WHITE);
        firstColor = new Color(Color.WHITE);
        thirdColor = new Color(Color.WHITE);
        super.setSize(205.0f, 205.0f);
        lines = new Array<Line>();
        this.region = region;
        segmentSize = 205.0f;
        isDirty = true;
        space = 50.0f;
    }

    public Color getFirstColor() {
        return firstColor;
    }

    public Color getSecondColor() {
        return secondColor;
    }

    public float getSpace() {
        return space;
    }

    public Color getThirdColor() {
        return thirdColor;
    }

    public void setFirstColor(@NotNull Color firstColor) {
        this.firstColor = firstColor;
    }

    @Override
    public void setHeight(float height) {
        if (height < 0.0f)
            height = 0.0f;
        float extra;
        float rest;
        if (height > segmentSize) {
            height -= segmentSize;
            float other = segmentSize - LINE_SIZE;
            rest = height % other;
            extra = rest > EPSILON ? other - rest : 0.0f;
            height += segmentSize;
        } else {
            rest = height % segmentSize;
            extra = rest > EPSILON ? segmentSize - rest : 0.0f;
        }
        super.setHeight(height >= segmentSize ? height + extra : segmentSize);
        isDirty = true;
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        isDirty = true;
    }

    public void setSecondColor(@NotNull Color secondColor) {
        this.secondColor = secondColor;
    }

    @Override
    public void setSize(float width, float height) {
        setHeight(height);
        setWidth(width);
    }

    /**
     * @param space [0.0, {@link Float#MAX_VALUE}]
     */
    public void setSpace(float space) {
        this.space = Math.max(0.0f, Math.min(space, Float.MAX_VALUE));
        segmentSize = 4 * this.space + 5 * LINE_SIZE;
        setSize(getWidth(), getHeight());
        isDirty = true;
    }

    public void setThirdColor(@NotNull Color thirdColor) {
        this.thirdColor = thirdColor;
    }

    @Override
    public void setWidth(float width) {
        if (width < 0.0f)
            width = 0.0f;
        float extra;
        float rest;
        if (width > segmentSize) {
            width -= segmentSize;
            float other = segmentSize - LINE_SIZE;
            rest = width % other;
            extra = rest > EPSILON ? other - rest : 0.0f;
            width += segmentSize;
        } else {
            rest = width % segmentSize;
            extra = rest > EPSILON ? segmentSize - rest : 0.0f;
        }
        super.setWidth(width >= segmentSize ? width + extra : segmentSize);
        isDirty = true;
    }

    @Override
    public void setX(float x) {
        isDirty = true;
        super.setX(x);
    }

    @Override
    public void setY(float y) {
        isDirty = true;
        super.setY(y);
    }

    @Override
    protected void render(@NotNull Batch batch, float parentAlpha) {
        for (Line line : lines) {
            Vector2 position = line.start;
            Color lineColor = line.color;
            Vector2 size = line.end;
            batch.setColor(lineColor.r, lineColor.g, lineColor.b, lineColor.a * parentAlpha);
            batch.draw(region, position.x, position.y, size.x, size.y);
        }
    }

    @Override
    protected void update(float delta) {
        calculate();
    }

    private void calculate() {
        if (isDirty) {
            freeOldLines();
            createVerticalLines();
            createHorizontalLines();
            isDirty = false;
        }
    }

    private void createHorizontalLines() {
        float height = getHeight();
        float width = getWidth();
        float nextY = getY();
        float y = nextY;
        float x = getX();
        int counter = 0;
        while (true) {
            Line line = ZPool.Obtain(Line.class);
            if (line != null) {
                line.color = spezifyColor(++counter);
                line.end.set(width, LINE_SIZE);
                line.start.set(x, nextY);
                nextY = space + nextY + LINE_SIZE;
                lines.add(line);
                if (nextY + LINE_SIZE - y > height)
                    break;
                continue;
            }
            break;
        }
    }

    private void createVerticalLines() {
        float height = getHeight();
        float width = getWidth();
        float nextX = getX();
        float x = nextX;
        float y = getY();
        int counter = 0;
        while (true) {
            Line line = ZPool.Obtain(Line.class);
            if (line != null) {
                line.color = spezifyColor(++counter);
                line.end.set(LINE_SIZE, height);
                line.start.set(nextX, y);
                nextX = space + nextX + LINE_SIZE;
                lines.add(line);
                if (nextX + LINE_SIZE - x > width)
                    break;
                continue;
            }
            break;
        }
    }

    private void freeOldLines() {
        for (Line line : lines)
            ZPool.Free(line);
        lines.clear();
    }

    private Color spezifyColor(int counter) {
        if (counter <= 5) {
            int value = counter % 5;
            switch (value) {
                case 0:
                    return firstColor;
                case 1:
                    return firstColor;
                case 2:
                    return secondColor;
                case 3:
                    return thirdColor;
                case 4:
                    return secondColor;
                default:
            }
        } else {
            int value = counter % 4;
            switch (value) {
                case 0:
                    return secondColor;
                case 1:
                    return firstColor;
                case 2:
                    return secondColor;
                case 3:
                    return thirdColor;
                default:
            }
        }
        return Color.WHITE;
    }
}
