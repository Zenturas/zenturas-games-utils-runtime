/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.zenturas.games.utils.runtime.entities2D;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.zenturas.games.utils.runtime.anotations.NotNull;

/**
 * @version 0.0.5.2
 */
public class ZSprite2D extends ZAbstractEntityPS2D {
    private TextureRegion region;

    public ZSprite2D(@NotNull Texture texture) {
        this(new TextureRegion(texture));
    }

    public ZSprite2D(@NotNull TextureRegion region) {
        super();
        init(region);
    }

    public TextureRegion getTextureRegion() {
        return region;
    }

    public void setTexture(@NotNull Texture texture) {
        init(new TextureRegion(texture));
    }

    public void setTextureRegion(@NotNull TextureRegion region) {
        init(region);
    }

    @Override
    protected void render(@NotNull Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(region, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    protected void update(float delta) {
    }

    private void init(@NotNull TextureRegion region) {
        setSize(region.getRegionWidth(), region.getRegionHeight());
        this.region = region;
    }
}
